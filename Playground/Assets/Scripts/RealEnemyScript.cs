﻿using UnityEngine;
using System.Collections;

public class RealEnemyScript : MonoBehaviour
{
	private Rigidbody body;
	public bool Force = true;
	public float velocity = 5f;

	public GameObject controller;
	public Transform player;
	private Vector3 playerpos;
	private Vector3 moveDir;
	private GameObject tempParticles;
	private GameplayController gameController;
	// Use this for initialization
	void Awake()
	{
		gameController = controller.GetComponent<GameplayController>();
		body = gameObject.GetComponent<Rigidbody>();

		playerpos = player.position;
		body.AddForce(new Vector3(playerpos.x, playerpos.y, playerpos.z), ForceMode.Impulse);
		//colParticle = controller.GetComponent<ParticleSystem>();
	}

	void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player")
			gameController.RespawnPlayer();
	}

	void Update()
	{
		transform.LookAt(player);

		transform.position += transform.forward * velocity * Time.deltaTime;
	}
	void FixedUpdate()
	{
		//body.MovePosition(moveDir);
	}

	//IEnumerator CleanUp()
	//{
	//}
}
