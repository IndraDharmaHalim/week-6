﻿ using UnityEngine;
using System.Collections;

public class EnemyScript: MonoBehaviour 
{
	private Rigidbody body;
	public bool randomForce = true;
	public float velocity = 10f;

	public GameObject controller;
	public GameObject colParticle;
	private GameObject tempParticles;
	private GameplayController gameController;
	// Use this for initialization
	void Awake () 
	{
		gameController = controller.GetComponent<GameplayController>();
		body = gameObject.GetComponent<Rigidbody>();
		body.AddForce(new Vector3(25, 0, 25), ForceMode.Impulse);
        //colParticle = controller.GetComponent<ParticleSystem>();
    }

    void OnCollisionEnter(Collision col)
	{
		if(col.gameObject.tag == "Player")
			gameController.RespawnPlayer();
		
		if(col != null)
			if (colParticle != null)
			{
				tempParticles = (GameObject)Instantiate(colParticle, transform.position, Quaternion.identity);
				tempParticles.GetComponent<ParticleSystem>().Play();
			}
		Vector3 reflection = body.velocity.normalized + ( 2 * (Vector3.Dot(body.velocity.normalized, col.contacts[0].normal)) * col.contacts[0].normal);
		body.velocity = reflection * velocity;



	}

	IEnumerator CleanUp()
	{
		yield return new WaitForSeconds(tempParticles.GetComponent<ParticleSystem>().duration);
		Destroy((Object)tempParticles);
	}
}
